@extends('layouts.app')
 
@section('title')
    <h1 class="text-center">{{ $title }}</h1>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $title }}</div>
                <br>
                <ul>
                    @if(count($services)>0)
                        @foreach($services as $service)
                        <li>{{$service}}</li>
                        @endforeach
                    @endif 
                </ul>
                <div class="panel-body">
                    This is the services page
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
