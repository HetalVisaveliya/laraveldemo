@extends('layouts.app') 

@section('content') 
<a href="/" class="btn btn-default pull-right">Go Back</a>
<table>
    <tr>
        <th><b>Username:</b></th>
        <td>{{ucfirst($user->name)}}</td>
    </tr>
    <tr>
        <th><b>Email:</b></th>
        <td>{{$user->email}}</td>
    </tr>
    <tr>
        <th><b>Registerd at:</b></th>
        <td>{{date('d/m/Y h:i A',strtotime($user->created_at))}}</td>
    </tr>
</table> 
@endsection  
 