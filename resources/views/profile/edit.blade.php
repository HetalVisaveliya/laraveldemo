@extends('layouts.app') 

@section('content') 
    <h1>Edit User</h1>   
    {!! Form::open(['action' => ['ProfileController@update', $user->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
         {{ Form::label('name', 'Username') }} 
         {{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Username' ]) }} 
    </div>
    <div class="form-group">
         {{ Form::label('email', 'Email') }} 
         {{ Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Email' ]) }} 
    </div> 
    <div class="form-group">
         {{Form::hidden('_method','PUT')}}
         {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}  
    </div>
    {!! Form::close() !!} 
@endsection
 