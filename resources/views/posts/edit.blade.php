@extends('layouts.app') 
 
@section('content')
    <h1>Edit Post</h1>   
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
         {{ Form::label('title', 'Title') }} 
         {{ Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title' ]) }} 
    </div>
    <div class="form-group">
         {{ Form::label('body', 'Body') }} 
         {{ Form::textarea('body', $post->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text' ]) }} 
    </div>
    <div class="form-group">
         {{ Form::file('cover_image') }}  
    </div> 
    <div class="form-group">
         {{Form::hidden('_method','PUT')}}
         {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}  
    </div>
    {!! Form::close() !!} 
    
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
@endsection  

 