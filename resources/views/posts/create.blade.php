@extends('layouts.app') 
 
@section('content')
    <h1>Create Post</h1>   
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
         {{ Form::label('title', 'Title') }} 
         {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title' ]) }} 
    </div>
    <div class="form-group">
         {{ Form::label('body', 'Body') }} 
         {{ Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text' ]) }} 
    </div>
    <div class="form-group">
         {{ Form::file('cover_image') }}  
    </div>
    <div class="form-group">
         {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}  
    </div>
    {!! Form::close() !!} 
    
    
 <!-- Scripts -->
<script src="/js/app.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'article-ckeditor' );
</script>
@endsection  
