<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use App\Post;

use Event;

// if want to featch record using sql query
use DB;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
//         try {
//             $posts = Post::where('titless','Title One')->get(); 
//
//            } catch (Illuminate\Database\QueryException $e) {
//                dd($e->getMessage());
//
//            } catch (PDOException $e) {
//                dd($e->getMessage());
//            }      
            
        try { 
        $posts = Post::where('title','Title One')->get(); 
    } catch(\Illuminate\Database\QueryException $ex){ 
        dd($ex->getMessage());
        //dd('her','she','him');
        echo $ex->getMessage(); die;
        // Note any method of class PDOException can be called on $ex.
}
        // $posts = Post::all(); 
        //$posts = Post::where('title','Post One')->get(); 
        //$posts = Post::orderBy('title','desc')->take(1)->get();
        // if want to featch record using sql query
//        $query = DB::table('posts')
//            ->where('title', '=', 'Post One')
//            ->orWhere(function($query)
//            {
//                $query->where('id', '>', 1)
//                      ->where('title', '<>', '4');
//            });
         //$sql = $query->toSql();  
         //$query = str_replace(array('?'), array('\'%s\''), $query->toSql());
          //  dd($posts);
    
   // $query = Post::where('title','Post One');
//         $query = str_replace(array('?'), array('\'%s\''), $query->toSql());
        //$query = $query->getBindings();
   // var_dump($query);
//    Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//        
//    var_dump($query->sql);
//    var_dump($query->bindings);
//    var_dump($query->time);
//});
       // var_dump($query->toSql());
        //echo '<br/>';
       // var_dump($query->getBindings());
//var_dump($query->toSql());
//var_dump($query->getBindings());
//
//DB::enableQueryLog();
//$posts = DB::select('select * from users where id = ?', [2]);
//$query = str_replace(array('?'), array('\'%s\''), $builder->toSql());
//$query = DB::getQueryLog();
//$query = end($query);
  
        
      //  dd($posts);
//print_r($query);

//DB::enableQueryLog();
//        $posts = DB::select('SELECT * FROM posts');
//        dd(DB::getQueryLog());
        //$posts = Post::orderBy('id','desc')->paginate(5);
        //dd($posts);
      //  $posts = Post::orderBy('title','desc')->toSql();
        //dd($posts);
        
    // $posts = Post::where('user_id', '=', 1)->get();
//        dd($posts);
         //DB::connection()->enableQueryLog();
       // $query = Post::where('title','Post One')->get(); 
       
//$query = DB::getQueryLog();
//$lastQuery = end($query);
//        $query = DB::table('posts')->where('title','Post One');
//        
//   $query1 =  str_replace(array('?'), array('\'%s\''), $query->toSql()); 
//   $sql = $query->getBindings();
          
 //$query = $query->getBindings();
  // print_r($query1);print_r($sql);
   
//    $txt = sprintf($query1,$sql[0]);
//    print_r($txt);
//print_r( $query->getBindings() );

// Event::listen('illuminate.query', function($query, $bindings)
// { 
//     echo 'TETE';
//     array_walk($bindings, function($value) use ($query)
//     {
//         preg_replace('/\?/', $value, $query, 1);
//    });
// 
//    var_dump($query);
// });
 //var_dump($query);
//print_r($lastQuery);
        return view('posts.index')->with('posts',$posts);
        //return "<br/>".'success';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        global $fileNameToStore;
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1991',
        ]);
        //Handle file upload
        if($request->hasFile('cover_image')){
            //Get file name with extentation 
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            //Get just filename
             $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extention
            $extention = $request->file('cover_image')->getClientOriginalExtension();
            //Filenameto Store
            $fileNametoStore = $filename.'_'.time().'.'.$extention;
            $file = $request->file('cover_image');
            $file->move(custom_storage_path('storagefile\cover_images\\'),$fileNametoStore);     
        }else{
            $fileNametoStore = 'noimage.png';
        }  
        //Create post
        $post = new Post;
        $post->user_id = auth()->user()->id;
        $post->title = $request->input('title');
        $post->body  = $request->input('body'); 
        $post->cover_image  = $fileNametoStore; 
        $post->save();
        
        return redirect('/posts')->with('success','Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $post = Post::find($id);
       return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
       $post = Post::find($id);
       //Check for correct user
       if(auth()->user()->id !== $post->user_id){
           return redirect('posts')->with('error','Unauthorised page');
       }
       return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        //Handle file upload
        if($request->hasFile('cover_image')){
            //Get file name with extentation 
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            //Get just filename
             $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extention
            $extention = $request->file('cover_image')->getClientOriginalExtension();
            //Filenameto Store
             $fileNametoStore = $filename.'_'.time().'.'.$extention;
            //Upload image
            $path = $request->file('cover_image')->storeAs('public/cover_images',$fileNametoStore);
        } 
        
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body  = $request->input('body'); 
        if($request->hasFile('cover_image')){
            $post->cover_image  = $fileNametoStore; 
        }
        $post->save();
        
        return redirect('/posts')->with('success','Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(auth()->user()->id !== $post->user_id){
            return redirect('posts')->with('error','Unauthorised page');
        } 
        if($post->cover_image != 'noimage.png'){
            Storage::delete('public/cover_images/'.$post->cover_image);
        }
        $post->delete();
        return redirect('/posts')->with('success','Post Deleted');        
    }
    
    public function testDelete(){
        echo "dd";
    }
}
