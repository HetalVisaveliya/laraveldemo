<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
 

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('profile.show')->with('user',$user);
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find(auth()->user()->id);
        return view('profile.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email', 
        ]);
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email  = $request->input('email'); 
        $user->save();
        
        return redirect('/')->with('success','User Updated');
    }
 
}
