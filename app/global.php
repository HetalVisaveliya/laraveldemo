<?php

if (!function_exists('print_data')) {
    function print_data($data,$break = true)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";

        if($break === true) exit;
    }
}

if (!function_exists('custom_storage_path')) {
    function custom_storage_path($path)
    {
        $ds = DIRECTORY_SEPARATOR;
        $bind_path = trim($path);
        return base_path()."{$ds}..{$ds}{$bind_path}";
    }
}


 