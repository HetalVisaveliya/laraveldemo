<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'PagesController@index'); 
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

Route::get('/viewprofile', 'ProfileController@index');

Route::resource('profile', 'ProfileController');
 
Auth::routes(); 

Route::get('/dashboard', 'DashboardController@index');
  
Route::resource('posts','PostsController');

Route::get('/profile','ProfileController@index')->middleware('auth.basic');
 
Route::get('/products', 'ProductsController@readItems');
Route::post('addItem', 'ProductsController@addItem');
Route::post('editItem', 'ProductsController@editItem');
Route::post('deleteItem', 'ProductsController@deleteItem');
 
Route::post('upload', 'CropController@postUpload');
Route::post('crop', 'CropController@postCrop');
